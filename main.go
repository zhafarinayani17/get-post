package main

import (
	"encoding/json"
	"fmt"
	"log"

	// "io/ioutil"
	"net/http"
)

type Article struct {
	Title string `json:"title"`
	Desc  string `json:"desc"`
}

type Articles []Article

var articles = Articles{
	Article{Title: "judul pertama", Desc: "Deskripsi pertama"},
	Article{Title: "judul kedua", Desc: "Deskripsi kedua"},
}

func main() {

	//middleware

	http.HandleFunc("/", getHome)
	http.HandleFunc("/articles", getArticles)
	http.HandleFunc("/post-articles", withLogging(postArticle))
	http.ListenAndServe(":3000", nil)
}

func getHome(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte("Nama Saya Alyani Zhafarina"))
}

func getArticles(w http.ResponseWriter, r *http.Request) {

	json.NewEncoder(w).Encode(articles)
}

func postArticle(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// body, err := ioutil.ReadAll(r.Body)
		// if err != nil {
		// 	http.Error(w, "Can't read body", http.StatusInternalServerError)
		// }
		// //Masukan database berhasil
		// w.Write([]byte(string(body)))

		var newArticle Article
		err := json.NewDecoder(r.Body).Decode(&newArticle)

		if err != nil {
			fmt.Printf("Ada Error, err")
		}

		articles = append(articles, newArticle)
		json.NewEncoder(w).Encode(articles)

		// //MELAKUKAN SESUATU
	} else {
		http.Error(w, "Invalid Request Methode", http.StatusMethodNotAllowed)
	}
}

func withLogging(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//kegunanan dari midellare
		log.Printf("Logged koneksi dari", r.RemoteAddr)
		next.ServeHTTP(w, r)
	}
}
